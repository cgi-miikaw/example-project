# Example Project

This repository contains some code that needs to get into Production

## Introduction

### Colors

The code in this repository makes up a `Colors` service. The main idea of this service is to tell the user the color of the day! An important detail, especially when selecting the attire for the day. Or flowers.

The service is made up from a few different parts:

There's a [frontend](frontend/), something that the end user can see on their screen. The frontend talks with a [server](frontend/server/), that in turn asks the color from a [backend](backend/). The backend is in contact with a database that holds information on all the available colors.

Here's a basic architectural diagram:

```mermaid
graph LR;
  frontend-->server;
  server-->backend;
  backend-->database;
```

When accessed, the service should look like this:

![Service](service.png)

### Version Control and Workflow

You're probably reading this from [Gitlab](https://gitlab.com/polarsquad/masters-of-dev) or an already cloned repository, so it's safe to assume you know we're using [Git](https://git-scm.com/) for this. :)

Please make sure you have Forked this repository!

### Frontend

The frontend is a small [React](https://reactjs.org/) application. Those that have a keen eye, notice that it's pretty close to what you get from the `create-react-app` command.

There would be multiple ways to serve this to the user, but this repository contains a [NodeJS](https://nodejs.org/en/) [Express](https://expressjs.com/) server that presents an API that the frontend app talks to.

This API is in fact just a passthrough connected to the backend, so you won't see much of an implementation [here](frontend/server/index.js). This could be a good place to implement any future things like user authentication for an admin portal or connections to other services.

### Backend

The backend is written in [Golang](https://golang.org/). It initializes a database, [Redis](https://redis.io/) in this case, and serves a small API for retrieving, adding and deleting the colors. Adding and deleting is currently not exposed through the frontend.

The code contains [Swagger](https://swagger.io/) annotations, and [swag](https://github.com/swaggo/swag) is used to automatically generated [documentation](backend/docs/) during the build process.
